class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :nome
      t.string :email
      t.string :telefone
      t.string :endereco
      t.boolean :medico

      t.timestamps null: false
    end
  end
end
