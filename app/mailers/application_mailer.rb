class ApplicationMailer < ActionMailer::Base
  default from: "noreply@panacea-patchworks.c9.io"
  layout 'mailer'
end