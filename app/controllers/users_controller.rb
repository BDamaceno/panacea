class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update]
  before_action :correct_user,   only: [:edit, :update]
  
  def index
    @users = User.find_by_especializacao(params[:especializacao])
    @users = User.paginate(page: params[:page])
  end

  def show
    @user = User.find(params[:id])
    @users = User.all
    @historicos = Historico.find_by_user_id(params[:id])
  end
  
  def new
    @user = User.new
  end
  
  def new_m
    @user = User.new
  end
  
  def activate
  @user = User.find(params[:id])
  if @user.update_attribute(:activated, true)
    redirect_to root_url
  else
    flash[:danger] = "Houve um erro com a validação do usuário"
   render root_url
  end 
  end
  
  def ativar_medico
    @user = User.find(params[:id])
    @user.activated = true
    if @user.save 
      flash[:info] = "Usuário registrado como médico"
     # render 'show'
    end
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      #@user.login
      @user.send_activation_email
      flash[:info] = "Bem-vindo a Panacea!"
      redirect_to root_url
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end
  
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
     flash[:success] = "Perfil atualizado."
     redirect_to @user
    else
      render 'edit'
    end
  end
  
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Você precisa estar conectado."
        redirect_to login_url
      end
    end
    
    
  
  private

    def user_params
      params.require(:user).permit(:nome, :email, :endereco, :telefone, :password,
                                   :password_confirmation, :tipo, :crm, :especializacao, :activated)
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user) || current_user.tipo == "A"
    end
end
