# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151117020651) do

  create_table "doencas", force: :cascade do |t|
    t.string   "nome"
    t.string   "descricao"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "sintomas",       default: "{}"
    t.string   "especializacao"
  end

  create_table "historicos", force: :cascade do |t|
    t.time     "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "busca"
    t.string   "user_id"
  end

  create_table "sintomadoencas", force: :cascade do |t|
    t.integer  "sintoma_id"
    t.integer  "doenca_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "sintomadoencas", ["doenca_id"], name: "index_sintomadoencas_on_doenca_id"
  add_index "sintomadoencas", ["sintoma_id"], name: "index_sintomadoencas_on_sintoma_id"

  create_table "sintomas", force: :cascade do |t|
    t.string   "nome"
    t.string   "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "nome"
    t.string   "email"
    t.string   "telefone"
    t.string   "endereco"
    t.boolean  "medico"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.string   "especializacao"
    t.string   "activation_digest"
    t.boolean  "activated",         default: true
    t.datetime "activated_at"
    t.string   "crm"
    t.string   "tipo"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
