class AddSintomasToDoencas < ActiveRecord::Migration
  def change
    add_column :doencas, :sintomas, :string, array: true, default: '{}'
  end
end
