class CreateHistoricos < ActiveRecord::Migration
  def change
    create_table :historicos do |t|
      t.time :data

      t.timestamps null: false
    end
  end
end
