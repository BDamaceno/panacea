class SintomasController < ApplicationController
    autocomplete :sintoma, :nome
   
    def show
     @sintoma = Sintoma.find(params[:id])
    
     #@doencas = @sintoma.doencas
     #@doencas = Doenca.with_sintomas(Sintoma.find(1,2)) #1,2 default
    end

   # def search
#     busca = [params[:sintoma_1]]  
#     busca = 1
#     @doencas = Doenca.with_sintomas(Sintoma.find(1,2)) #1,2 default
#    end

    

    def index
      @sintomas = Sintoma.search(params[:search])
      @historico = Historico.new
      
      @historico.busca = params
      @historico.user_id = current_user.id
      @historico.save
      #@historico.busca = params([:sintoma_ids]).to_s
      @doencas = Doenca.with_sintomas(params[:sintoma_ids])
    end


    
    def new
        @sintoma = Sintoma.new
    end

    def create
        @sintoma = Sintoma.new(sintoma_params)
        if @sintoma.save
            flash[:success] = "Adicionado com sucesso"
            redirect_to :back
        else
            render 'new'
        end
    end
    
    private

    def sintoma_params
      params.require(:sintoma).permit(:nome, :descricao)
    end

    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Você precisa estar conectado."
        redirect_to login_url
      end
    end
end