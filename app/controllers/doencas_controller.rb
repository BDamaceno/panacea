class DoencasController < ApplicationController
 before_action :logged_in_user, only: [:new, :create]
 before_action :user_type, only: [:new, :create, :update, :edit]
 
 
  @sintoma=Sintoma.new
  @sintomadoencas = Sintomadoenca.new
  
  def show
    @doenca = Doenca.find_by_id(params[:id])
    @sintomadoencas = Sintomadoenca.find_by_doenca_id(params[:id])
    @users = User.all
  end

  def index
    @doenca = Doenca.paginate(page: params[:page])
  end

  def new
    @doenca = Doenca.new
  end
  
  def create
    @doenca = Doenca.new(doenca_params)
    if @doenca.save
      flash[:success] = "Adicionado com sucesso"
      redirect_to @doenca
    else
      render 'new'
    end
  end
  
  #####
  def update
    @doenca = Doenca.find(params[:id])
    @sintomadoenca = Sintomadoenca.find(params[:id])
    if @doenca.update_attributes(doenca_params)
     flash[:success] = "Atualizado."
     redirect_to @doenca
    else
      render 'edit'
    end
  end
  
  def edit
    @doenca = Doenca.find(params[:id])
  end
  #####
  
  private

    def doenca_params
      params.require(:doenca).permit(:nome, :descricao, :especializacao)
      #, :sintoma
    end

    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Você precisa estar conectado."
        redirect_to login_url
      end
    end
    
    def user_type
      #@user = User.find(params[:id])
      unless current_user.tipo == "M" || current_user.tipo == "A"
        flash[:danger] = "Você não possui autorização para realizar essa ação."
        redirect_to index_d_url
      end
    end

end

