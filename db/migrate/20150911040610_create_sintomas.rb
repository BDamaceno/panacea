class CreateSintomas < ActiveRecord::Migration
  def change
    create_table :sintomas do |t|
      t.string :nome
      t.string :descricao

      t.timestamps null: false
      
    end
  end
end
