class ActivateUsersController

  def update
    @user = User.find(params[:id])
    if @user.activate
      flash[:info] = "Success"
    end
    redirect_to @user
  end
end