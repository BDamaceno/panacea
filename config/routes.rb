Rails.application.routes.draw do
  resources :users do
   member do
      get 'activate'
     put 'activate'
   end
  end

  resources :sintomadoencas, shallow: true do
    resources :doencas
  end

get "users/:id/activate" => "users#activate", :as => "active_user"
  
  get 'doencas/new'

  get 'sessions/new'

  root             'static_pages#home'
  get 'help'    => 'static_pages#help'
  get 'about'   => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
  
  get 'signup'  => 'users#new'
  get 'm_signup'=> 'users#new_m'
  get 'edit'    => 'users#edit'
  #post 'users/ativar_medico'
  
  
  
  get 'add'     => 'doencas#new'
  get 'index_d' => 'doencas#index'
  get 'edit'    => 'doencas#edit'
  
  get 'add_sintoma' => 'sintomas#new'
  get 's_index' => 'sintomas#index'
  #get 'busca' => 'sintomas#search'
  get 'edit' => 'sintomas#edit'
  
  resources :account_activations, only: [:edit]

  resources :users
  
  resources :doencas 
  resources :sintomas
  resources :sintomadoencas
  
  get 'sd' => 'sintomadoencas#new' 
  get 'search' => 'sintomadoencas#search'
  get 'remove' => 'sintomadoencas#delete'
  
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

end
