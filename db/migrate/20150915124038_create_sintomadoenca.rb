class CreateSintomadoenca < ActiveRecord::Migration
  def change
    create_table :sintomadoencas do |t|
      t.belongs_to :sintoma, index: true
      t.belongs_to :doenca, index: true
      t.timestamps null: false
    end
  end
end
