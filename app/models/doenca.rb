class Doenca < ActiveRecord::Base
    has_many :sintomadoencas#, dependent: :destroy, inverse_of: :ilness
    has_many :sintomas, through: :sintomadoencas

    scope :with_sintomas, ->(sintomas) { joins(sintomadoencas: :sintoma).where("sintomas.id in (?)", sintomas).group("doencas.id")
                                        .having("count(doencas.id) >= ?", (sintomas.present? ? sintomas.length : 0) ) }
    
    validates :nome, uniqueness: true,  presence: true, length: { maximum: 50 }
    validates :descricao, presence:true, length: {minimum: 150} 
    
    def self.for_sintomas(*s)
        Doenca.scoped.join(:sintomas).merge(Sintoma.sintoma(s))
    end
  
end
