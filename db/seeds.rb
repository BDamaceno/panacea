User.create!(nome:  "Bruno da Costa Damaceno",
             email: "damacenob@hotmail.com",
             endereco: "Rua José Bonifácio 68",
             telefone: "30343468",
             password:              "foobar",
             password_confirmation: "foobar")

99.times do |n|
  nome  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  endereco = "Rua José #{n+1}º"
  telefone = "0500-3#{n+99}"
  password = "password"
  User.create!(nome:  nome,
               email: email,
               telefone: telefone,
               endereco: endereco,
               password:              password,
               password_confirmation: password)
end