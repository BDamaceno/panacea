class Sintoma < ActiveRecord::Base
    has_many :sintomadoencas
    has_many :doencas, through: :sintomadoencas
    
    validates :nome,  presence: true, uniqueness: true, length: { maximum: 50 }
   # validates :descricao, presence:true, length: {minimum: 20} 
   
   
   def sintomas(*args)
     where("sintomas.sintoma in (?)" , args.flatten.compact.uniq)
   end

    def self.search(search)
        if search
            where( 'nome LIKE ?', "%#{search}%").all
        else
            all
        end
    end


end
