class SintomadoencasController < ApplicationController
 before_action :logged_in_user, only: [:new, :create]
 @doenca = Doenca.new
 @sintoma = Sintoma.new
 
  def new
    @sintomadoenca = Sintomadoenca.new
    @sintoma = Sintoma.new
  end
  
   def search
    @sintoma = Sintoma.find(sintoma_id).doencas
    @historico = Historico.new
    
   end
  
  def create
        @sintomadoenca = Sintomadoenca.new(sintomadoenca_params)
        @testes = Sintomadoenca.all
        @testes.each do |teste|
        if @sintomadoenca.sintoma_id == teste.sintoma_id && @sintomadoenca.doenca_id == teste.doenca_id
            flash[:danger] = "Esse sintoma já foi relacionado à essa doença."
            break
           else if @sintomadoenca.save
              @doenca = Doenca.find_by_id(@sintomadoenca.doenca_id)
              
              #redirect_to @doenca
              #else
             #render 'new'
            end
        end
      end
      redirect_to :back
  end
   
   def show
   @sintomadoenca = Sintomadoenca.find_by_id(params[:id])
   @doenca = Doenca.find_by_id(params[:doenca_id])
   @sintoma = Sintoma.find_by_id(params[:sintoma_id])
   # @sintomadoenca = Sintomadoenca.includes(:doenca,:sintoma).where(id: params[:id])
   end
    
   def delete
    @sintomadoenca = Sintomadoenca.find_by(sintomadoenca_params)
    @sintomadoenca.destroy
   end
    
    private
    def sintomadoenca_params
      params.require(:sintomadoenca).permit(:doenca_id, :sintoma_id)
    end
    
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Você precisa estar conectado."
        redirect_to login_url
      end
    end

end